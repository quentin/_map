# How to efficiently provide email access to our users?

## JMAP

specs:

  * [rfc8620](https://tools.ietf.org/html/rfc8620) The JSON Meta Application Protocol (JMAP)
  * [rfc8621](https://tools.ietf.org/html/rfc8621) The JSON Meta Application Protocol (JMAP) for Mail
  * [specs](https://jmap.io/spec.html) JMAP: The Specs

clients:

  * [ltt.rs](https://github.com/iNPUTmice/lttrs-android) Android Client
  * [meli](https://meli.delivery/) Rust CLI client
  * [cypht](https://github.com/jasonmunro/cypht) PHP+JS webmail

## IMAP

specs:

  * [rfc3501](https://tools.ietf.org/html/rfc3501) INTERNET MESSAGE ACCESS PROTOCOL - VERSION 4rev1
  * [rfc4466](https://tools.ietf.org/html/rfc4466) Collected Extensions to IMAP4 ABNF
  * [rfc4469](https://tools.ietf.org/html/rfc4469) Internet Message Access Protocol (IMAP) CATENATE Extension
  * [rfc4551](https://tools.ietf.org/html/rfc4551) IMAP Extension for Conditional STORE Operation or Quick Flag Changes Resynchronization
  * [rfc5032](https://tools.ietf.org/html/rfc5032) WITHIN Search Extension to the IMAP Protocol
  * [rfc5182](https://tools.ietf.org/html/rfc5182) IMAP Extension for Referencing the Last SEARCH Result
  * [rfc5738](https://tools.ietf.org/html/rfc5738) IMAP Support for UTF-8
  * [rfc6186](https://tools.ietf.org/html/rfc6186) Use of SRV Records for Locating Email Submission/Access Services
    * Can be used by the alps webmail
  * [rfc6858](https://tools.ietf.org/html/rfc6858) Simplified POP and IMAP Downgrading for Internationalized Email
  * [rfc7817](https://tools.ietf.org/html/rfc7817) Updated Transport Layer Security (TLS) Server Identity Check Procedure for Email-Related Protocols
  * [rfc8314](https://tools.ietf.org/html/rfc8314) Cleartext Considered Obsolete: Use of Transport Layer Security (TLS) for Email Submission and Access
  * [rfc8437](https://tools.ietf.org/html/rfc8437) IMAP UNAUTHENTICATE Extension for Connection Reuse
  * [rfc8474](https://tools.ietf.org/html/rfc8474) IMAP Extension for Object Identifiers

## Exchange ActiveSync (EAS)

[[MS-ASHTTP]: Exchange ActiveSync: HTTP Protocol](https://docs.microsoft.com/en-us/openspecs/exchange_server_protocols/ms-ashttp/4cbf28dc-2876-41c6-9d87-ba9db86cd40d)

## Existing projetcs that could help us

  * [maddy](https://foxcpp.dev/maddy/) full-featured easy-to-setup email server. IMAP storage is "beta".
	* [go-emersion](https://github.com/emersion/go-imap/) a client and server IMAP implementation. The client implem is used by alps.
